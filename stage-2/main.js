;
(function($) {

  // Change port event handlers
  function addChangePortEventHandlers(direction1, direction2, column1, column2) {
    $(`#${direction1}-port`).on('change', optionsHandler);
    $(`#${direction2}-port`).on('change', optionsHandler);

    function optionsHandler() {
      const el1 = $(`#${direction1}-port option:selected`);
      const el2 = $(`#${direction2}-port option:selected`);
      const selected1 = el1.text();
      const selected2 = el2.text();
      const showAll = (el1.val() == 0) && (el2.val() == 0);
  
      $.each($('.table tbody tr'), (i, val) => {
        const isCurrentCell1 = selected1 === $($(val).children()[column1 - 1]).text();
        const isCurrentCell2 = selected2 === $($(val).children()[column2 - 1]).text();
        
        if ((selected1 === selected2) && !showAll) {
          $(val).hide();
          return;
        } else if (
          showAll ||
          (isCurrentCell1 && isCurrentCell2) ||
          (isCurrentCell1 !== isCurrentCell2)
          ) {
          $(val).show();
        } else {
          $(val).hide();
        }
      })
    }
  }

  addChangePortEventHandlers('from', 'to', 1, 3);

  // Menu handler
  $('#hide-menu').on('click', function() {
    $('.menu, .container').toggleClass('toggled', 500);
    const span = $('.menu li span');
    if ($('.menu').hasClass('toggled')) {
      span.show(500);
    } else {
      span.hide(500);
    }
  })

  // Mobile menu handler
  $('#sandwich').on('click', function() {
    $('.menu').toggle('slide', { direction: 'left' }, 500);
  })

  // Fix bug with resize and hidden menu
  $( window ).resize(function() {
    if ($(document).width() >= 768) {
      $('.menu, .hide-menu').show();
    } else {
      $('.menu, .hide-menu').hide();
    }
  });

  // Scroll to ship handler
  const scrollTo = (ship) => {
    $(`#${ship}`).on('click', function() {
      $('html, body').animate({
        scrollTop: ($(`#${ship}-anchor`).offset().top)
      }, 500);
    });
  }

  scrollTo('vilnius');
  scrollTo('kaunas');
  
})(jQuery);
