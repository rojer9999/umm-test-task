import React, { useState } from 'react';

import CountryList from '../CountryList';
import MainInput from '../MainInput';
import countries from './countries.json';

const App = () => {
  const [text, changeText] = useState('');
  const [countryList, changeCountryList] = useState([]);

  const onChangeCountryList = (txt) => {
    let matchCountry = [];

    if (txt !== '') {
      matchCountry = countries.filter((country) => {
        const uprCountry = country.toUpperCase();
        const uprTxt = txt.toUpperCase();

        return uprCountry.includes(uprTxt) && uprCountry !== uprTxt;
      });
    }

    changeCountryList(matchCountry);
  };

  const onSelectCountry = (country) => {
    changeText(country);
    onChangeCountryList(country);
  };

  const onChangeText = ({ target: { value } }) => onSelectCountry(value);

  return (
    <div className="app">
      <a className="logo" href="../../../stage-2">
        <span>Stage 2</span>
      </a>
      <MainInput text={text} onChangeText={onChangeText} />
      <CountryList countryList={countryList} onSelectCountry={onSelectCountry} />
    </div>
  );
};

export default App;
