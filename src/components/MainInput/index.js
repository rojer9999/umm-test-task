import React from 'react';

const MainInput = ({ text, onChangeText }) => (
  <input className="main-input" value={text} onChange={onChangeText} />
);

export default MainInput;
