import React from 'react';

const CountryList = ({ countryList, onSelectCountry }) => (
  <div className="country-list">
    {
      countryList.map((country) => (
        <button
          className="country"
          type="button"
          onClick={() => onSelectCountry(country)}
          key={country}
        >
          {country}
        </button>
      ))
    }
  </div>
);

export default CountryList;
